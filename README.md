# ELEC260 "BopIt"
A game designed for a MECH206 laboratory session I did (designed to spec as per marking guidelines).

You must press the button when the green LED is lit to advance to the next level. If you hit the button when either of the yellow LEDs are lit, you will not advance and will get a chance to try again. If you hit the button when any of the red LEDs are lit, the game will reset, putting you back at the first level.

Your level, and game overs, are displayed in the serial monitor.

See  https://www.tinkercad.com/things/flZazVPA0zH for an interactive version.
