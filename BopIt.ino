const int button_pin = 13;
const int leds[] = {
  2,3,4, //RRR
  5,6,7, //YGY
  8,9,10 //RRR
};
int level;
int pos;


void setup() {
  Serial.begin(9600);
  for (int i = 0; i < sizeof(leds); i++) pinMode(leds[i], OUTPUT);
  pinMode(button_pin, INPUT);
  NewGame();
}

void loop() {
  //Advance
  SetPins(0, LOW);
  SetPins(1, LOW);
  SetPins(2, LOW);
  if (pos == 9) pos = -8;
  pos ++;
  digitalWrite(leds[abs(pos)], HIGH);
  delay(100);

  
  if (digitalRead(button_pin) == LOW){ //Check Button Press
    if (abs(pos) == 4){ //Green LED
      level ++;
      Serial.print("Level "); Serial.println(level);
      Blink(0);
    }
    else if (abs(pos) == 3 or abs(pos) == 5){ //Yellow LEDs
      Blink(1);
    }
    else{ //Red LEDs
      Serial.println("Game Over");
      Blink(2);
      NewGame();
    }
  }
}

void NewGame(){
  level = 1;
  pos = 0;
  delay(1000);
}

void Blink(int c){ //Flashes the LEDs rapidly 5 times and then waits for a moment.
  for(int i = 0; i < 5; i++){
    SetPins(c, HIGH);
    delay(100);
    SetPins(c, LOW);
    delay(100);
  }
  delay(500);
}

void SetPins(int c, int state){
  if (c == 0){ //Green LED
      digitalWrite(leds[4], state);
    }
    else if (c == 1){ //Yellow LEDs
      digitalWrite(leds[3], state);
      digitalWrite(leds[5], state);
    }
    else{ //Red LEDs
      digitalWrite(leds[0], state);
      digitalWrite(leds[1], state);
      digitalWrite(leds[2], state);
      digitalWrite(leds[6], state);
      digitalWrite(leds[7], state);
      digitalWrite(leds[8], state);
    }
}
